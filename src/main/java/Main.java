import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class Main {


    public static String get_comments(ArrayList<String[]> all_arr, int position) {
        String res = "";
        if (!all_arr.get(position)[0].equals("Utf8")) {

            for (int i = 1; i < all_arr.get(position).length; i++) {
                res += get_comments(all_arr, Integer.valueOf(all_arr.get(position)[i]) - 1);
            }

        } else {
            res += all_arr.get(position)[1];
        }
        return res;
    }

    public static void main(String[] args) {
        Map<Integer, String[]> hashMap = new HashMap<>();
        hashMap.put(7,  new String[]{"Class", "2"});
        hashMap.put(9,  new String[]{"Fieldref", "2", "2"});
        hashMap.put(10, new String[]{"Methodref", "2", "2"});
        hashMap.put(11, new String[]{"InterfaceMethodref", "2", "2"});
        hashMap.put(8,  new String[]{"String", "2"});
        hashMap.put(3,  new String[]{"Integer", "4"});
        hashMap.put(4,  new String[]{"Float", "4"});
        hashMap.put(5,  new String[]{"Long", "4", "4"});
        hashMap.put(6,  new String[]{"Double", "4", "4"});
        hashMap.put(12, new String[]{"NameAndType", "2", "2"});
        hashMap.put(1,  new String[]{"Utf8", "2", "[]"});
        hashMap.put(15, new String[]{"MethodHandle", "1", "2"});
        hashMap.put(16, new String[]{"MethodType", "2"});
        hashMap.put(17, new String[]{"Dynamic", "2", "2"});
        hashMap.put(18, new String[]{"InvokeDynamic", "2", "2"});
        hashMap.put(19, new String[]{"Module", "2"});
        hashMap.put(20, new String[]{"Package", "2"});

        ArrayList<String[]> all_arr = new ArrayList<>();

        try {
            FileInputStream in = new FileInputStream("src/main/resources/App.class");
            DataInputStream is = new DataInputStream(in);
            is.skipBytes(8);
            int const_pool_count = is.readUnsignedShort();
//            System.out.println(const_pool_count);


            for (int i = 0; i < const_pool_count - 1; i++) {
                int tag = is.readUnsignedByte();
                String[] arr = hashMap.get(tag);
//                System.out.print("#" + (i + 1) + " = " + arr[0]);
//                System.out.print("  ");
                int val = 0;
                String[] arr_to_add = new String[arr.length];
                arr_to_add[0] = arr[0];
                for (int j = 1; j < arr.length; j++) {
                    switch (arr[j]) {
                        case "2" -> {
                            val = is.readUnsignedShort();
                            if (tag != 1) {
                                arr_to_add[j] = Integer.toString(val);
//                                System.out.print("#" + val + " ");
                            }
                        }
                        case "4" -> {

                            val = is.readUnsignedShort();
                            arr_to_add[j] = Integer.toString(val);
//                            System.out.print("#" + val + " ");
                        }
                        case "[]" ->  arr_to_add[j - 1] = new String(is.readNBytes(val), StandardCharsets.UTF_8);
//                                System.out.print(new String(is.readNBytes(val), StandardCharsets.UTF_8));
                    }
                }
                all_arr.add(arr_to_add);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < all_arr.size(); i++) {
            String[] r = all_arr.get(i);
            System.out.print("#" + i + 1 + " = " + r[0] + " ");
            for (int j = 1; j < r.length; j++) {
                if (j == r.length - 1 && r[0] == "Utf8") {
                    break;
                }
                System.out.print("#" + r[j] + " ");
            }
            if (!all_arr.get(i)[0].equals("Utf8")) {
                System.out.println("// " + get_comments(all_arr, i));
            } else {
                System.out.println();
            }
        }
    }
}
